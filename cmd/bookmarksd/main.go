package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/antizealot1337/bookmarks"
	"github.com/julienschmidt/httprouter"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

var (
	addr       = ":9005"
	driver     = "sqlite3"
	connection = ":memory:"
)

func init() {
	flag.StringVar(&addr, "address", addr, "The address to listen on")
	flag.StringVar(&driver, "driver", driver, "The database driver")
	flag.StringVar(&connection, "source", connection, "The database connection string")

	flag.Parse()
} //func

func main() {
	log.Println("Database:", driver, "Source:", connection)

	db, err := bookmarks.OpenSQLDatabase(driver, connection)
	if err != nil {
		log.Fatal(err)
	} //if
	defer db.Close()

	if driver == "sqlite3" && connection == ":memory:" {
		log.Println("Initializing in memory database")

		err = db.Initialize(context.Background())
		if err != nil {
			log.Fatal(err)
		} //if
	} //if

	r := httprouter.New()

	r.GET("/", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		switch r.Header.Get("Accept") {
		case "application/json":
			categories, err := db.Categories(r.Context(), true)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			data, err := json.Marshal(categories)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			w.Write(data) // Ignore any write errors
		default:
			categories, err := db.Categories(r.Context(), true)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			err = idxTmpl.Execute(w, categories)
			if err != nil {
				log.Println("[template]", err)
			} //if
		} //switch

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.POST("/categories/new", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		name := r.PostFormValue("name")
		if name == "" {
			msg := "missing form value: name"
			http.Error(w, msg, http.StatusBadRequest)
			log.Println(r.URL.Path, msg, http.StatusBadRequest)
			return
		} //if

		c := bookmarks.Category{
			Name: name,
			Desc: r.PostFormValue("description"),
		}

		err := db.CategorySave(r.Context(), &c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		if r.URL.Query().Get("redirect") != "" {
			u := "/"
			http.Redirect(w, r, u, http.StatusSeeOther)
			log.Println(r.URL.Path, "=>", u, http.StatusSeeOther)
			return
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.GET("/category/:slug/edit", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		category, err := db.CategoryBySlug(r.Context(), p.ByName("slug"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		err = categoryEditTmpl.Execute(w, category)
		if err != nil {
			fmt.Println("[template]", err)
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.POST("/category/:slug/save", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		name := r.PostFormValue("name")
		if name == "" {
			msg := "missing value: name"
			http.Error(w, msg, http.StatusBadRequest)
			log.Println(r.URL.Path, msg, http.StatusBadRequest)
			return
		} //if

		c, err := db.CategoryBySlug(r.Context(), p.ByName("slug"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		c.Name = name
		c.Desc = r.PostFormValue("description")

		err = db.CategorySave(r.Context(), &c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		if r.URL.Query().Get("redirect") != "" {
			u := fmt.Sprintf("/category/%s", c.Slug)
			http.Redirect(w, r, u, http.StatusSeeOther)
			log.Println(r.URL.Path, "=>", u, http.StatusSeeOther)
			return
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.POST("/category/:slug/delete", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		name := p.ByName("slug")

		err := db.CategoryDeleteSlug(r.Context(), name)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		if r.URL.Query().Get("redirect") != "" {
			u := "/"
			http.Redirect(w, r, u, http.StatusSeeOther)
			log.Println(r.URL.Path, "=>", u, http.StatusSeeOther)
			return
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.GET("/category/:slug", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		switch r.Header.Get("Accept") {
		case "application/json":
			category, err := db.CategoryBySlug(r.Context(), p.ByName("slug"))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			data, err := json.Marshal(category)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			w.Write(data) // Ignore any write errors
		default:
			category, err := db.CategoryBySlug(r.Context(), p.ByName("slug"))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			err = categoryTmpl.Execute(w, category)
			if err != nil {
				fmt.Println("[template]", err)
			} //if
		} //switch

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.POST("/bookmarks/new", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		categoryID, err := strconv.ParseInt(r.FormValue("categoryId"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println(r.URL.Path, err, http.StatusBadRequest)
			return
		} //if

		link := r.FormValue(("url"))
		if link == "" {
			msg := "missing from value: link"
			http.Error(w, msg, http.StatusBadRequest)
			log.Println(r.URL.Path, msg, http.StatusBadRequest)
			return
		} //if

		err = db.BookmarkSave(r.Context(), &bookmarks.Bookmark{
			CategoryID: categoryID,
			Link:       link,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		if r.URL.Query().Get("redirect") != "" {
			c, err := db.CategoryByID(r.Context(), categoryID)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			u := fmt.Sprintf("/category/%s", c.Slug)
			http.Redirect(w, r, u, http.StatusSeeOther)
			log.Println(r.URL.Path, u, http.StatusSeeOther)
			return
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	r.POST("/bookmark/:id/delete", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		id, err := strconv.ParseInt(p.ByName("id"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println(r.URL.Path, err, http.StatusBadRequest)
			return
		} //if

		b, err := db.BookmarkByID(r.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		err = db.BookmarkDelete(r.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(r.URL.Path, err, http.StatusInternalServerError)
			return
		} //if

		if r.URL.Query().Get("redirect") != "" {
			c, err := db.CategoryByID(r.Context(), b.CategoryID)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(r.URL.Path, err, http.StatusInternalServerError)
				return
			} //if

			u := fmt.Sprintf("/category/%s", c.Slug)
			http.Redirect(w, r, u, http.StatusSeeOther)
			log.Println(r.URL.Path, u, http.StatusSeeOther)
			return
		} //if

		log.Println(r.URL.Path, http.StatusOK)
	})

	log.Println("Listening on", addr)

	if err := http.ListenAndServe(addr, r); err != nil {
		log.Fatal(err)
	} //if
} //func

const idxTmplSrc = `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bookmarks</title>
		<style>
body {
	font-family: SANS-SERIF;
	margin: 0px;
}

header {
	background-color: blue;
	color: white;
	margin: 0px;
	min-height: 80px;
}

input {
	font-size: 1.2em;
}

input[type=text] {
	border-top-width: 0px;
	border-left-width: 0px;
	border-right-width: 0px;
	border-color: pink;
}

input[type=submit] {
	background-color: pink;
	color: white;
	border: none;
	border-radius: 5px;
	min-height: 40px;
	min-width: 60px;
}

form.new {
	display: flex;
	justify-content: space-between;
}

.med_pad {
	padding: 16px;
}

.card {
	border: outset;
	border-radius: 10px;
	max-width: 1000px;
}

.center {
	margin-left: auto;
	margin-right: auto;
}
		</style>
	</head>
	<body>
		<header class="med_pad">
			<h1>Bookmark Categories</h1>
		</header>
		<form class="new med_pad" action="/categories/new?redirect=true" method="POST">
			<input style="flex-grow: 1;" type="text" name="name" placeholder="Name..." required>
			<span style="min-width: 20px;"></span>
			<input style="flex-grow: 1;" type="text" name="description" placeholder="Description...">
			<span style="min-width: 20px;"></span>
			<input type="submit" value="New">
		</form>
		<div class="categories med_pad">
{{ range . }}
			<div class="center card med_pad">
				<h3><a href="/category/{{ .Slug }}">{{ .Name }}</a></h3>
				<p>
					{{ .Desc }}
				</p>
			</div>
{{ end }}
		</div>
	</body>
</html>
`

var idxTmpl = template.Must(template.New("index").Parse(idxTmplSrc))

const categoryTmplSrc = `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Category - {{ .Name }}</title>
		<style>
body {
	font-family: SANS-SERIF;
	margin: 0px;
}

header {
	background-color: blue;
	color: white;
	margin: 0px;
	min-height: 80px;
}

input {
	font-size: 1.2em;
}

input[type=text] {
	border-top-width: 0px;
	border-left-width: 0px;
	border-right-width: 0px;
	border-color: pink;
}

input[type=submit] {
	background-color: pink;
	color: white;
	border: none;
	border-radius: 5px;
	min-height: 40px;
	min-width: 60px;
}

a.button {
	font-size: 1.2em;
	text-decoration: none;
	padding-top: 1px;
	padding-bottom: 1px;
	padding-left: 6px;
	padding-right: 6px;
	margin: 6px;
}

.button {
	background-color: blue;
	color: white;
	border-radius: 5px;
	min-height: 40px;
	min-width: 60px;
}

form.new {
	display: flex;
	justify-content: space-between;
}

.med_pad {
	padding: 16px;
}

.card {
	border: outset;
	border-radius: 10px;
	max-width: 1000px;
}

.center {
	margin-left: auto;
	margin-right: auto;
}
		</style>
	</head>
	<body>
		<header class="med_pad" style="display: flex;">
			<a style="color: white; text-decoration: none; padding-top: 30px; font-size: larger;" href="/">←</a>
			<span style="width: 20px;"></span>
			<h1 style="display: inline; flex-grow: 1">{{ .Name }}</span>
		</header>
		<div class="med_pad">
			<form style="display: inline;" action="/category/{{ .Slug }}/edit" method="GET">
				<input type="submit" value="Edit">
			</form>
			<form style="display: inline;" action="/category/{{ .Slug }}/delete?redirect=true" method="POST">
				<input type="submit" value="Delete">
			</form>
		</div>
		<form class="new med_pad" action="/bookmarks/new?redirect=/category/{{ .Slug }}" method="POST">
			<input type="hidden" name="categoryId" value="{{ .ID }}">
			<input style="flex-grow: 1;" type="text" name="url" placeholder="Link..." required>
			<span style="width: 20px"></span>
			<input type="submit" value="New">
		</form>
		<div class="bookmarks">
{{ range .Bookmarks }}
			<div style="display: flex;" class="card center med_pad">
				<a style="font-size: large;" href="{{ .Link }}" target="_blank">{{ .Link }}</a>
				<span style="flex-grow: 1;"></span>
				<form style="display: inline" action="/bookmark/{{ .ID }}/delete?redirect=true" method="POST">
					<input type="submit" value="Delete">
				</form>
			</div>
{{ end }}
		</div>
	</body>
</html>
`

var categoryTmpl = template.Must(template.New("category view").Parse(categoryTmplSrc))

const categoryEditTmplSrc = `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Cateogry - Edit: {{ .Name }}</title>
		<link href="/style.css" rel="stylesheet"></link>
		<style>
body {
	font-family: SANS-SERIF;
	margin: 0px;
}

header {
	background-color: blue;
	color: white;
	margin: 0px;
	min-height: 80px;
}

input {
	font-size: 1.2em;
}

input[type=text] {
	border-top-width: 0px;
	border-left-width: 0px;
	border-right-width: 0px;
	border-color: pink;
}

input[type=submit] {
	background-color: pink;
	color: white;
	border: none;
	border-radius: 5px;
	min-height: 40px;
	min-width: 60px;
}

a.button {
	font-size: 1.2em;
	text-decoration: none;
	padding-top: 1px;
	padding-bottom: 1px;
	padding-left: 6px;
	padding-right: 6px;
	margin: 6px;
}

.button {
	background-color: blue;
	color: white;
	border-radius: 5px;
	min-height: 40px;
	min-width: 60px;
}

form.new {
	display: flex;
	justify-content: space-between;
}

.med_pad {
	padding: 16px;
}

.card {
	border: outset;
	border-radius: 10px;
	max-width: 1000px;
}

.center {
	margin-left: auto;
	margin-right: auto;
}
		</style>
	</head>
	<body>
		<header class="med_pad">
			<a style="font-size: larger;" class="button" href="/category/{{ .Slug }}">←</a>
			<h1 style="display: inline-flex;">Edit: {{ .Name }}</h1>
		</header>
		<div style="height: 10px;"></div>
		<form style="display: flex;" class="new med_pad" action="/category/{{ .Slug }}//save?redirect=true" method="POST">
			<input style="flex-grow: 1;" type="text" name="name" placeholder="Name..." value="{{ .Name }}" required>
			<span style="width: 20px;"></span>
			<input style="flex-grow: 1;" type="text" name="description" placeholder="Description..." value="{{ .Desc }}">
			<span style="width: 20px;"></span>
			<input type="submit" value="Save">
		</form>
	</body>
</html>
`

var categoryEditTmpl = template.Must(template.New("category edit").Parse(categoryEditTmplSrc))
