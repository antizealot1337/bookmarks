package main

import (
	"context"
	"flag"
	"fmt"
	"strconv"

	"bitbucket.org/antizealot1337/bookmarks"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

var (
	driver string
	source string
)

func init() {
	flag.StringVar(&driver, "driver", driver, "The sql driver")
	flag.StringVar(&source, "source", source, "The database source")

	flag.Usage = func() {
		out := flag.CommandLine.Output()

		fmt.Fprintln(out, "bookmarksdb")
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Description: Command line bookmarks database management tool")
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Usage:")
		fmt.Fprintln(out, "  bookmarksdb [flags] init                                    Initialize the database")
		fmt.Fprintln(out, "  bookmarksdb [flags] category|c add    <name> <description>  Add a category")
		fmt.Fprintln(out, "  bookmarksdb [flags] category|c delete <name>                Delete a category")
		fmt.Fprintln(out, "  bookmarksdb [flags] category|c list                         List the categories")
		fmt.Fprintln(out, "  bookmarksdb [flags] category|c show   <slug>                List the categories")
		fmt.Fprintln(out, "  bookmarksdb [flags] bookmark|b add    <categoryId> <url>    Add a bookmark.")
		fmt.Fprintln(out, "  bookmarksdb [flags] bookmark|b delete <id>                  Delete a bookmark")
		fmt.Fprintln(out, "  bookmarksdb [flags] bookmark|b list   <categoryId>          List bookmarks for category")
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Flags:")
		flag.PrintDefaults()
	} //func

	flag.Parse()
} //func

func main() {
	if flag.NArg() == 0 {
		panic("error: no command")
	} //if

	switch cmd := flag.Arg(0); cmd {
	case "init", "i":
		db, err := bookmarks.OpenSQLDatabase(driver, source)
		if err != nil {
			panic(err)
		} //if
		defer db.Close()

		err = db.Initialize(context.Background())
		if err != nil {
			panic(err)
		} //if
	case "category", "c":
		db, err := bookmarks.OpenSQLDatabase(driver, source)
		if err != nil {
			panic(err)
		} //if
		defer db.Close()

		switch act := flag.Arg(1); act {
		case "add":
			name := flag.Arg(2)
			if name == "" {
				panic("no name provided")
			} //if

			c := bookmarks.Category{
				Name: name,
				Desc: flag.Arg(3),
			}

			err = db.CategorySave(context.Background(), &c)
			if err != nil {
				panic(err)
			} //if
		case "delete":
			slg := flag.Arg(2)
			if slg == "" {
				panic("no slug provided")
			} //if

			err = db.CategoryDeleteSlug(context.Background(), slg)
			if err != nil {
				panic(err)
			} //if
		case "list":
			categories, err := db.Categories(context.Background(), false)
			if err != nil {
				panic(err)
			} //if

			for _, category := range categories {
				fmt.Printf("ID: %d Name: \"%s\" Slug: \"%s\" Desc: \"%s\"\n", category.ID, category.Name, category.Slug, category.Desc)
			} //for
		case "show":
			slg := flag.Arg(2)
			if slg == "" {
				panic("no slug provided")
			} //if

			category, err := db.CategoryBySlug(context.Background(), slg)
			if err != nil {
				panic(err)
			} //if

			fmt.Println("ID:", category.ID)
			fmt.Println("Name:", category.Name)
			fmt.Println("Slug:", category.Slug)
			fmt.Println("Desc:", category.Desc)
			if len(category.Bookmarks) > 0 {
				fmt.Println("Bookmarks:")
				for _, bookmark := range category.Bookmarks {
					fmt.Printf("  ID: %d URL: %s\n", bookmark.ID, bookmark.Link)
				} //for
			} //if
		default:
			panic("bad action: " + act)
		} //switch
	case "bookmark", "b":
		db, err := bookmarks.OpenSQLDatabase(driver, source)
		if err != nil {
			panic(err)
		} //if
		defer db.Close()

		switch act := flag.Arg(1); act {
		case "add":
			id, err := strconv.ParseInt(flag.Arg(2), 10, 64)
			if err != nil {
				panic(err)
			} //if

			u := flag.Arg(3)
			if u == "" {
				panic("no url provided")
			} //if

			err = db.BookmarkSave(context.Background(), &bookmarks.Bookmark{
				CategoryID: id,
				Link:       u,
			})
			if err != nil {
				panic(err)
			} //if
		case "delete":
			id, err := strconv.ParseInt(flag.Arg(2), 10, 64)
			if err != nil {
				panic(err)
			} //if

			err = db.BookmarkDelete(context.Background(), id)
			if err != nil {
				panic(err)
			} //if
		default:
			panic("bad action" + act)
		} //switch
	default:
		panic("bad command: " + cmd)
	} //switch
} //func
