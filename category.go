package bookmarks

// Category is a grouping of Bookmarks.
type Category struct {
	ID        int64      `json:"id"`
	Name      string     `json:"name"`
	Desc      string     `json:"description"`
	Slug      string     `json:"slug"`
	Bookmarks []Bookmark `json:"bookmarks,ommitempty"`
} //struct
