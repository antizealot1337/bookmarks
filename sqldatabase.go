package bookmarks

import (
	"context"
	"database/sql"

	"github.com/gosimple/slug"
)

// SQLDatabase write sql.DB to provide functionality to persist Categories and
// Bookmarks.
type SQLDatabase struct {
	db *sql.DB
} //struct

// OpenSQLDatabase will open a sql.DB and create a new SqlDB. It will return any
// errors from opening.
func OpenSQLDatabase(driver, source string) (*SQLDatabase, error) {
	db, err := sql.Open(driver, source)
	if err != nil {
		return nil, err
	} //if

	return NewSQLDatabase(db), nil
} //func

// NewSQLDatabase create a new SdlDB.
func NewSQLDatabase(db *sql.DB) *SQLDatabase {
	return &SQLDatabase{
		db: db,
	}
} //func

// Initialize creates tables in the sql.DB. It will drop any existing tables
// and data.
func (d *SQLDatabase) Initialize(ctx context.Context) (err error) {
	var tx *sql.Tx
	tx, err = d.db.BeginTx(ctx, nil)
	if err != nil {
		return
	} //if

	defer func() {
		if err != nil {
			tx.Rollback()
		} //if
	}() //func

	_, err = d.db.Exec("DROP TABLE IF EXISTS categories;")
	if err != nil {
		return
	} //if

	_, err = d.db.Exec(`CREATE TABLE categories (
_id integer primary key autoincrement,
name text unique not null,
description text not null,
slug text unique not null
);`)
	if err != nil {
		return
	} //if

	_, err = d.db.Exec("DROP TABLE IF EXISTS bookmarks;")
	if err != nil {
		return
	} //if

	_, err = d.db.Exec(`CREATE TABLE bookmarks (
_id integer primary key autoincrement,
categoryId integet not null,
url text not null,
FOREIGN KEY(categoryId) REFERENCES categories(_id) ON DELETE CASCADE
);`)

	err = tx.Commit()

	return
} //func

// Categories will return all the Categories. If true is provided it will
// return the Bookmarks as well for the Categories.
func (d *SQLDatabase) Categories(ctx context.Context, full bool) ([]Category, error) {
	rows, err := d.db.QueryContext(ctx, "SELECT _id, name, description, slug FROM categories;")
	if err != nil {
		return nil, err
	} //if
	defer rows.Close()

	categories := []Category{}

	for rows.Next() {
		var category Category
		err := rows.Scan(&category.ID, &category.Name, &category.Desc, &category.Slug)
		if err != nil {
			return categories, err
		} //if

		if full {
			category.Bookmarks, err = d.categoryBookmarksByID(ctx, category.ID)
			if err != nil {
				return categories, nil
			} //if
		} //if

		categories = append(categories, category)
	} //for

	return categories, nil
} //func

// CategoryByID returns the category with the provided id.
func (d *SQLDatabase) CategoryByID(ctx context.Context, id int64) (*Category, error) {
	row := d.db.QueryRowContext(ctx, "SELECT name, description, slug FROM categories WHERE _id=?", id)

	c := Category{
		ID: id,
	}

	err := row.Scan(&c.Name, &c.Desc, &c.Slug)
	if err != nil {
		return nil, err
	} //if

	return &c, nil
} //func

// CategorySave will save the Category. If the ID is 0 then the Category will
// be added. After a successful insertion the Category ID field will be set.
// If the ID is greater than 0 then the existing Category will be updated.
func (d *SQLDatabase) CategorySave(ctx context.Context, c *Category) error {
	c.Slug = slug.Make(c.Name)

	if c.ID == 0 {
		res, err := d.db.ExecContext(ctx, "INSERT INTO categories (name, description, slug) VALUES (?, ?, ?);", c.Name, c.Desc, c.Slug)
		if err != nil {
			return err
		} //if

		c.ID, err = res.LastInsertId()
		if err != nil {
			return err
		} //if
	} else {
		_, err := d.db.ExecContext(ctx, "UPDATE categories SET name=?, description=?, slug=? WHERE _id=?", c.Name, c.Desc, c.Slug, c.ID)
		if err != nil {
			return err
		} //if
	} //if

	return nil
} //func

// CategoryDeleteSlug will delete a category using the slug.
func (d *SQLDatabase) CategoryDeleteSlug(ctx context.Context, slug string) (err error) {
	tx, err := d.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	} //if
	defer func() {
		if err != nil {
			tx.Rollback()
		} //if
	}() //func

	row := tx.QueryRowContext(ctx, "SELECT _id from categories WHERE slug=?", slug)

	var id int64
	err = row.Scan(&id)
	if err != nil {
		return
	} //if

	_, err = tx.Exec("DELETE FROM categories WHERE _id=?", id)
	if err != nil {
		return
	} //if

	_, err = tx.Exec("DELETE FROM bookmarks WHERE categoryId=?", id)
	if err != nil {
		return
	} //if

	err = tx.Commit()

	return
} //func

// CategoryBySlug will return the Category by its slug.
func (d *SQLDatabase) CategoryBySlug(ctx context.Context, slug string) (Category, error) {
	row := d.db.QueryRowContext(ctx, "SELECT _id, name, description FROM categories WHERE slug=?;", slug)

	c := Category{
		Slug: slug,
	}
	err := row.Scan(&c.ID, &c.Name, &c.Desc)
	if err != nil {
		return c, err
	} //if

	c.Bookmarks, err = d.categoryBookmarksByID(ctx, c.ID)
	if err != nil {
		return c, err
	} //if

	return c, nil
} //func

func (d *SQLDatabase) categoryBookmarksByID(ctx context.Context, id int64) ([]Bookmark, error) {
	rows, err := d.db.QueryContext(ctx, "SELECT _id, url FROM bookmarks WHERE categoryId=?;", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookmarks []Bookmark

	for rows.Next() {
		bookmark := Bookmark{
			CategoryID: id,
		}

		err = rows.Scan(&bookmark.ID, &bookmark.Link)
		if err != nil {
			return bookmarks, err
		} //if

		bookmarks = append(bookmarks, bookmark)
	} //for

	return bookmarks, nil
} //func

// BookmarkByID returns a bookmark from the given id.
func (d *SQLDatabase) BookmarkByID(ctx context.Context, id int64) (*Bookmark, error) {
	row := d.db.QueryRowContext(ctx, "SELECT _id, categoryId, url FROM bookmarks WHERE _id=?", id)

	b := Bookmark{
		ID: id,
	}

	err := row.Scan(&b.ID, &b.CategoryID, &b.Link)
	if err != nil {
		return nil, err
	} //if

	return &b, nil
} //func

// BookmarkSave will save the Bookmark.
func (d *SQLDatabase) BookmarkSave(ctx context.Context, bookmark *Bookmark) error {
	if bookmark.ID == 0 {
		res, err := d.db.ExecContext(ctx, "INSERT INTO bookmarks (categoryId, url) VALUES (?, ?);", bookmark.CategoryID, bookmark.Link)
		if err != nil {
			return err
		} //if

		bookmark.ID, err = res.LastInsertId()
		if err != nil {
			return err
		} //if
	} else {
		_, err := d.db.ExecContext(ctx, "UPDATE bookmarks SET categoryId=?, url=? WHERE _id=?", bookmark.CategoryID, bookmark.Link, bookmark.CategoryID)
		if err != nil {
			return err
		} //if
	} //if
	return nil
} //func

// BookmarkDelete will delete a Bookmark.
func (d *SQLDatabase) BookmarkDelete(ctx context.Context, id int64) (err error) {
	_, err = d.db.ExecContext(ctx, "DELETE FROM bookmarks WHERE _id=?;", id)
	return
} //func

// Close will close the underlying sql.DB.
func (d *SQLDatabase) Close() error {
	return d.db.Close()
} //func
