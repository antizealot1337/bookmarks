package bookmarks

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"github.com/gosimple/slug"
	_ "github.com/mattn/go-sqlite3"
)

func TestOpenSQLDatabase(t *testing.T) {
	db, err := OpenSQLDatabase("sqlite3", ":memory:")
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if
	defer db.Close()

	if db.db == nil {
		t.Fatal("*sql.DB not opened/present")
	} //if
} //func

func TestSQLDatabaseNewSQLDatabase(t *testing.T) {
	sqlitedb, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if
	defer sqlitedb.Close()

	db := NewSQLDatabase(sqlitedb)
	if db.db != sqlitedb {
		t.Error("expected the database passed to be set")
	} //if
} //func

func TestSQLDatabaseInitialize(t *testing.T) {
	db, err := OpenSQLDatabase("sqlite3", ":memory:")
	if err != nil {
		t.Error("unexpected error:", err)
	} //if
	defer func() {
		if err := db.Close(); err != nil {
			t.Error("unexpected error:", err)
		} //if
	}()

	err = db.Initialize(context.Background())
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	// TODO: Test for database tables
} //func

func TestSQLDatabaseClose(t *testing.T) {
	db, err := OpenSQLDatabase("sqlite3", ":memory:")
	if err != nil {
		t.Error("unexpected error:", err)
	} //if

	err = db.Close()
	if err != nil {
		t.Error("unexpected error")
	} //if
} //func

func TestSQLDatabaseActions(t *testing.T) {
	ctx := context.Background()
	tmpfile := filepath.Join(os.TempDir(),
		fmt.Sprintf("go-test-bookmarks-%d.db", time.Now().UnixNano()))

	db, err := OpenSQLDatabase("sqlite3", tmpfile)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if
	defer db.Close()
	defer os.Remove(tmpfile)

	if db.db == nil {
		t.Fatal("*sql.DB not opened/present")
	} //if

	err = db.Initialize(ctx)
	if err != nil {
		t.Error("unexpected error")
	} //if

	t.Run("Categories", func(t *testing.T) {
		err = db.CategorySave(ctx, &Category{Name: "Cat 1"})
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		err = db.CategorySave(ctx, &Category{Name: "Cat 2"})
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		categories, err := db.Categories(ctx, false)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 2, len(categories); actual != expected {
			t.Errorf(`expected %d categories but was %d`, expected, actual)
		} //if
	})

	t.Run("CategorySave", func(t *testing.T) {
		c := Category{
			Name: "Category 1",
			Desc: "The first category",
		}

		err = db.CategorySave(ctx, &c)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if c.ID == 0 {
			t.Error("expected Category.ID to be set (not 0)")
		} //if

		if expected, actual := slug.Make(c.Name), c.Slug; actual != expected {
			t.Errorf(`expected Category.Slug to be "%s" but was "%s"`, expected,
				actual)
		} //if
	})
	t.Run("CategoryByID", func(t *testing.T) {
		c1 := Category{
			Name: "Category By ID",
			Desc: "The first category",
		}

		err = db.CategorySave(ctx, &c1)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		c2, err := db.CategoryByID(ctx, c1.ID)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !reflect.DeepEqual(&c1, c2) {
			t.Errorf("expected Category (%+v) but got (%+v)", c1, c2)
		} //if
	})
	t.Run("CategoryBySlug", func(t *testing.T) {
		c1 := Category{
			Name: "Category By Slug",
			Desc: "The first category",
		}

		err = db.CategorySave(ctx, &c1)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		c2, err := db.CategoryBySlug(ctx, c1.Slug)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !reflect.DeepEqual(c1, c2) {
			t.Errorf("expected Category (%+v) but got (%+v)", c1, 2)
		} //if
	})
	t.Run("CategoryDeleteSlug", func(t *testing.T) {
		c1 := Category{
			Name: "Category Delete Slug",
			Desc: "The first category",
		}

		err = db.CategorySave(ctx, &c1)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		err := db.CategoryDeleteSlug(ctx, c1.Slug)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		_, err = db.CategoryByID(ctx, c1.ID)
		if err == nil {
			t.Error("expected error for accessing a Category that has been deleted")
		} //if
	})
	t.Run("categoryBookmarksByID", func(t *testing.T) {
		c := Category{
			Name: "Category Bookmarks By ID",
		}

		err := db.CategorySave(ctx, &c)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		t.Logf("%+v", c)

		b := Bookmark{
			CategoryID: c.ID,
			Link:       "http://example.com",
		}

		err = db.BookmarkSave(ctx, &b)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		t.Logf("%+v", b)

		bookmarks, err := db.categoryBookmarksByID(ctx, c.ID)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 1, len(bookmarks); actual != expected {
			t.Fatalf(`expected len([]Bookmark) to be %d but was %d`, expected, actual)
		} //if

		if !reflect.DeepEqual(b, bookmarks[0]) {
			t.Errorf("expected Bookmark (%+v) but got (%+v)", b, bookmarks[0])
		} //if
	})
	t.Run("BookmarkByID", func(t *testing.T) {
		c := Category{
			Name: "Category Bookmark By ID",
		}

		err := db.CategorySave(ctx, &c)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		b1 := Bookmark{
			Link: "http://example.com",
		}

		err = db.BookmarkSave(ctx, &b1)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		b2, err := db.BookmarkByID(ctx, b1.ID)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !reflect.DeepEqual(&b1, b2) {
			t.Errorf("expected Bookmark (%+v) but got (%+v)", &b1, b2)
		} //if
	})
	t.Run("BookmarkSave", func(t *testing.T) {
		c := Category{
			Name: "Category Bookmark Save",
		}

		err := db.CategorySave(ctx, &c)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		b := Bookmark{
			Link: "http://example.com",
		}

		err = db.BookmarkSave(ctx, &b)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if b.ID == 0 {
			t.Error("expected Bookmark.ID to not be 0")
		} //if
	})
	t.Run("BookmarkDelete", func(t *testing.T) {
		c := Category{
			Name: "Category Bookmark Delete",
		}

		err := db.CategorySave(ctx, &c)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		b := Bookmark{
			Link: "http://example.com",
		}

		err = db.BookmarkSave(ctx, &b)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		err = db.BookmarkDelete(ctx, b.ID)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if
	})
} //func
