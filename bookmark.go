package bookmarks

// Bookmark represents a url in a Category.
type Bookmark struct {
	ID         int64  `json:"id"`
	CategoryID int64  `json:"-"`
	Link       string `json:"link"`
} //struct
