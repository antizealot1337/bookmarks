module bitbucket.org/antizealot1337/bookmarks

go 1.14

require (
	github.com/gosimple/slug v1.9.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.5.2
	github.com/mattn/go-sqlite3 v1.13.0
)
