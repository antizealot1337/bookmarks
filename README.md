# Bookmarks

Bookmarks runs a web server for a centralized cross platform bookmark tracking
application. 

## API

There is a limited API that can be used to interact with it with interfaces
other than the HTML. Any request for data in an API fashion should request a
JSON response by via the header `Accept: application/json`. Any request to
modify data should use the **POST** HTTP verb and regular form encoding. Any
missing optional form data will be replaced with a _zero value_.

### Routes
| Method | Action | Description |
|--------|--------|-------------|
| GET | `/` | Returns the categories and their bookmarks. |
| POST | `/categories/new` | Adds a category. Requires _name_ in form data and optionally accepts _description_. |
| GET | `/category/<slug>` | Returns a single category and its bookmarks by its associated _slug_. |
| POST | `/category/<slug>/delete` | Delete the category and its bookmarks by its associated _slug_. |
| POST | `/category/<slug>/save` | Update the category with a _name_ value in form data associated with the _slug_. The category's slug will be updated based on the name provided. The form value _description_ is optionally accepted.|
| POST | `/bookmarks/new` | Adds a bookmark. It requires the _categoryId_ and _url_ form data. The _categoryId_ must be from an existing category. The _url_ is the link to save. |
| POST | `/bookmark/<id>/delete` | Delete a bookmark with the associated _id_. |